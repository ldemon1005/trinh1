<div class="modal-dialog">
    <div class="modal-content">
        <form method="post" action="{{route('admin_update_price_action')}}">
            {{csrf_field()}}
            <input hidden value="{{$price->id}}" name="id">
            <div class="modal-header">
                <h4 class="modal-title">Thêm khoảng giá điện</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Tên khoảng giá điện</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="name" value="{{$price->name}}"/>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Số điện min</label>
                    </div>
                    <div class="col-md-8">
                        <input name="min" class="form-control" value="{{$price->min}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Số điện max</label>
                    </div>
                    <div class="col-md-8">
                        <input name="max" class="form-control" value="{{$price->max}}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Giá</label>
                    </div>
                    <div class="col-md-8">
                        <input name="price" class="form-control" value="{{$price->price}}">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                <button type="submit" class="btn btn-primary btn-add-price">Update</button>
            </div>
        </form>
    </div>
</div>
