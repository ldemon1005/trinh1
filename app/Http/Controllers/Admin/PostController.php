<?php

namespace App\Http\Controllers\Admin;

use App\Models\BaseModel;
use App\Models\Post;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    //<editor-fold desc="bài hướng dẫn">
    public function postView(){
        $post = Post::where('type',Post::MANUAL)->first();
        return view('admin.post.form',compact('post'));
    }

    public function postUpdate(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required',
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $post_data = $request->all();

        $post_id = $request->get('id');

        unset($post_data['id']);

        try{
            $post = $this->postRepository->updatePost($post_id, $post_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        if(!$post){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        return redirect()->back()->with(BaseModel::ALERT_SUCCESS, 'Update thành công!');
    }
    //</editor-fold>

    //<editor-fold desc="loại thiết bị tiết kiệm điện">
    public function postView1(){
        $post = Post::where('type',Post::TYPE_ELECTRIC)->first();
        return view('admin.post.form1',compact('post'));
    }

    public function postUpdate1(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required',
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $post_data = $request->all();

        $post_id = $request->get('id');

        unset($post_data['id']);

        try{
            $post = $this->postRepository->updatePost($post_id, $post_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        if(!$post){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        return redirect()->back()->with(BaseModel::ALERT_SUCCESS, 'Update thành công!');
    }
    //</editor-fold>

    //<editor-fold desc="Lập phương án tiết kiệm điện">
    public function postView2(){
        $post = Post::where('type',Post::TYPE_PLAN_SAVE_ELECTRIC)->first();
        return view('admin.post.form2',compact('post'));
    }

    public function postUpdate2(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required',
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $post_data = $request->all();

        $post_id = $request->get('id');

        unset($post_data['id']);

        try{
            $post = $this->postRepository->updatePost($post_id, $post_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        if(!$post){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        return redirect()->back()->with(BaseModel::ALERT_SUCCESS, 'Update thành công!');
    }

}
