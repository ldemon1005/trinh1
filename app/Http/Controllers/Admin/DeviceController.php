<?php

namespace App\Http\Controllers\Admin;

use App\Models\BaseModel;
use App\Repositories\DeviceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DeviceController extends Controller
{
    protected $deviceRepository;

    public function __construct(DeviceRepository $deviceRepository)
    {
        $this->deviceRepository = $deviceRepository;
    }

    public function listView(Request $request){
        $params = [
            'keyword' => '',
        ];
        $query = $request->only('keyword', 'status');
        $params = array_merge($params, $query);
        if(isset($params['status']) && $params['status'] == 9){
            unset($params['status']);
        }
        $list_device = $this->deviceRepository->getList($params);
        return view('admin.device.list_view', compact('list_device','params'));
    }

    public function updateDeviceView($id){
        $device = $this->deviceRepository->getByID($id);

        if(!$device){
            $this->resFail(null, 'Không tìm thấy thiết bị');
        }

        $content = view('admin.device.form', compact('device'))->render();

        return $this->resSuccess($content, '');
    }

    public function createDevice(Request $request){
        $validator = Validator::make($request->input(), [
            'name' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $device_data = $request->only('name','status','create_from','capacity');
        if(isset($device_data['status']) && $device_data['status'] == 'on'){
            $device_data['status'] = 1;
        }else{
            $device_data['status'] = 0;
        }
        try{
            $device = $this->deviceRepository->createDeviceName($device_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Tạo mới không thành công!');
        }

        if(!$device){
            return redirect()->back()->withErrors('Tạo mới không thành công!');
        }
        if(isset($device_data['create_from']) && $device_data['create_from'] == 'client')
            return redirect()->route('view_calculator')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công, loại thiết bị bạn thêm cần được duyệt bởi admin!');
        return redirect()->route('admin_list_device')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công!');
    }

    public function updateDevice(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required',
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            redirect()->back()->withErrors($validator->errors());
        }

        $device_data = $request->only('id','name','status', 'capacity');

        $device_id = $request->get('id');

        unset($device_data['id']);

        if(isset($device_data['status']) && $device_data['status'] == 'on'){
            $device_data['status'] = 1;
        }else{
            $device_data['status'] = 0;
        }

        try{
            $device = $this->deviceRepository->updateDeviceName($device_id, $device_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        if(!$device){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        return redirect()->route('admin_list_device')->with(BaseModel::ALERT_SUCCESS, 'Update thành công!');
    }

    public function deleteDevice(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }
        $device_data = $request->only('id');

        try {
            $device_deleted = $this->deviceRepository->deleteDeviceName($device_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$device_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($device_deleted, 'Xoá thành công!');

    }
}
