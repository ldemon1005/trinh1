<div class="container">
    <header class="header">
        <div class="header-menu">
            <div class="nav-toggle" onclick="showMenuMobile()">
                <span class="fa fa-bars"></span>
            </div>
            <div class="header-menu-catg">
                <div class="menu-category">
                    <ul>
                        <li><a href="/">Trang chủ</a></li>
                        <li><a href="/tinh-toan-dien-nang">Tính toán điện năng tiêu thụ </a></li>
                        <li><a href="{{route('view_plan_save_electric')}}">Lập phương án tiết kiệm điện</a></li>
                        <li><a href="{{route('view_tb')}}">Tư vấn giải pháp tiết kiệm điện</a></li>
                        <li><a href="{{route('view_user_manual')}}">Hướng dẫn</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
</div>
