<?php

namespace App\Http\Controllers\Client;

use App\Models\BaseModel;
use App\Models\Post;
use App\Repositories\DeviceRepository;
use App\Repositories\PriceRepository;
use App\Repositories\TargetRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    protected $deviceRepository;
    protected $targetRepository;
    protected $priceRepository;
    public function __construct(DeviceRepository $deviceRepository, TargetRepository $targetRepository, PriceRepository $priceRepository)
    {
        $this->deviceRepository = $deviceRepository;
        $this->targetRepository = $targetRepository;
        $this->priceRepository  = $priceRepository;
    }

    public function index(){
        return view('client.index.index');
    }

    public function device(){
        $listDevice = $this->deviceRepository->getList(['status' => BaseModel::STATUS_ACTIVE], 1000);
        $listTarget = $this->targetRepository->getList(['status' => BaseModel::STATUS_ACTIVE], 1000);
        $listPrice  = $this->priceRepository->getList();
        if($listPrice) $listPrice  = json_encode($listPrice->toArray());
        else $listPrice = json_encode([]);

        if($listDevice) $listDevice1 = json_encode($listDevice->toArray());
        else $listDevice1 = json_encode([]);

        return view('client.index.device', compact('listDevice','listTarget','listPrice','listDevice1'));
    }

    public function userManual(){
        $post = Post::where('type',Post::MANUAL)->first();
        return view('client.index.user-manual',compact('post'));
    }
    public function userManual1(){
        $post = Post::where('type',Post::TYPE_ELECTRIC)->first();
        return view('client.index.user-manual-1',compact('post'));
    }
    public function userManual2(){
        $post = Post::where('type',Post::TYPE_PLAN_SAVE_ELECTRIC)->first();
        return view('client.index.user-manual-2',compact('post'));
    }
}
