@extends('client.master')

@section('content')
    <div id="content-main">
        <div class="container">
            <div class="navigator">
                <a href="/">Trang chủ</a>»<span>Hướng dẫn</span>
            </div>
            <session class="box-content">
                <div class="content-page">
                    {!! $post->content !!}
                </div>
            </session>
        </div>
    </div>
@endsection

