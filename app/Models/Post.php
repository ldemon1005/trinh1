<?php

namespace App\Models;

class Post extends BaseModel
{
    const MANUAL = 1; /* Hướng dẫn */
    const TYPE_ELECTRIC = 2; /* Tư vấn giải pháp tiết kiệm điện  */
    const TYPE_PLAN_SAVE_ELECTRIC = 3; /* Phương án tiết kiệm điện */

    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'posts';


}
