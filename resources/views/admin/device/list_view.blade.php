@extends('admin.master')

@section('content')
    <div>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Device
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Device</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row form-group">
                <form method="get" action="{{route('admin_list_device')}}" id="search-form">
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" name="keyword" value="{{$params['keyword']}}" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-primary">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <select class="form-control" onchange="$('#search-form').submit()" name="status">
                            <option value="9" selected>Tất cả</option>
                            <option {{isset($params['status']) && $params['status'] == 1 ? 'selected' : ''}} value="1">Hoạt động</option>
                            <option {{isset($params['status']) && $params['status'] == 0 ? 'selected' : ''}} value="0">Không hoạt động</option>
                        </select>
                    </div>
                </form>
                <div class="col-md-4 text-right">
                    <button type="button" id="add-device" class="btn btn-primary">Thêm mới</button>
                </div>
            </div>
            <table class="table table-bordered">
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Name</th>
                    <th style="width: 10%;text-align: center">Status</th>
                    <th style="width: 15%;text-align: center">Action</th>
                </tr>
                @foreach($list_device as $key => $device)
                    <tr>
                        <td>{{$key +1}}.</td>
                        <td>{{$device->name}}</td>
                        <td class="text-center">
                            <button type="button" class="btn btn-{{$device->status == 1 ? 'success' : 'danger'}} btn-rounded btn-sm" data-toggle="tooltip" title="{{$device->status == 1 ? 'Hoạt động' : 'Không hoạt động'}}">
                                <i class="fa fa-{{$device->status == 1 ? 'eye' : 'eye-slash'}}"></i>
                            </button>
                        </td>
                        <td class="text-center">
                            <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                                    data-device_id="{{$device->id}}">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-device_id="{{$device->id}}" data-toggle="tooltip" title="Xoá">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
                <div class="row form-group pull-right">
                    {{$list_device->links()}}
                </div>
            </table>
        </section>
        <!-- /.content -->

        <div class="modal" ref="device" id="device">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" action="{{route('admin_create_device')}}">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <h4 class="modal-title">Thêm loại thiết bị</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label>Tên, loại thiết bị</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="name"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label>Mức độ tiêu thụ</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="capacity"/>
                                </div>
                            </div>
                        </div>

                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label>Trạng thái</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" name="status" id="customSwitch1" checked>
                                        <label class="custom-control-label" for="customSwitch1">Hoạt động</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary btn-add-device">Thêm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal" ref="device" id="update-device">
        </div>
    </div>
@endsection

@section('admin_script')
    <script>
        $(document).ready(function () {
            $('#add-device').on('click', function (e) {
                $('#device').modal();
            });

            $('.btn__delete').on("click", function (e) {
                let device_id = $(this).attr("data-device_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_delete_device')}}",
                        data: {
                            id: device_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            console.log(xhr);
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });

            $('.btn__edit').on("click", function (e) {
                let device_id = $(this).attr("data-device_id");
                $.ajax({
                    type: "GET",
                    url:  "update-device/" + device_id,
                    dataType: "json",
                    success: function (result) {
                        if (result.code === 1) {
                            $('#update-device').html(result.data);
                            $('#update-device').modal();
                        } else {
                            toastError(result.msg);
                        }
                    },
                    error: function (xhr) {
                        toastError(xhr.responseJSON.msg);
                    }
                });
            });
        });
    </script>
@endsection
