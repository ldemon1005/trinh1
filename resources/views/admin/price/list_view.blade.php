@extends('admin.master')

@section('content')
    <div>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Price
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Price</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row form-group">
                {{--<form method="get" action="{{route('admin_list_price')}}" id="search-form">
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" name="keyword" value="{{$params['keyword']}}" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-primary">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <select class="form-control" onchange="$('#search-form').submit()" name="status">
                            <option value="9" selected>Tất cả</option>
                            <option {{isset($params['status']) && $params['status'] == 1 ? 'selected' : ''}} value="1">Hoạt động</option>
                            <option {{isset($params['status']) && $params['status'] == 0 ? 'selected' : ''}} value="0">Không hoạt động</option>
                        </select>
                    </div>
                </form>--}}
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-danger">Xóa config</button>
                    <button type="button" id="add-price" class="btn btn-primary">Thêm mới</button>
                </div>
            </div>
            <table class="table table-bordered">
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Name</th>
                    <th style="width: 15%;text-align: center">Số điện min</th>
                    <th style="width: 15%;text-align: center">Số điện max</th>
                    <th style="width: 15%;text-align: center">Giá</th>
                    <th style="width: 15%;text-align: center">Action</th>
                </tr>
                @foreach($list_price as $key => $price)
                    <tr>
                        <td>{{$key +1}}.</td>
                        <td>{{$price->name}}</td>
                        <td class="text-center">{{$price->min}}</td>
                        <td class="text-center">{{$price->max}}</td>
                        <td class="text-center">{{$price->price}}</td>
                        <td class="text-center">
                            <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                                    data-price_id="{{$price->id}}">
                                <i class="fa fa-pencil"></i>
                            </button>
                            {{--<button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-price_id="{{$price->id}}" data-toggle="tooltip" title="Xoá">
                                <i class="fa fa-trash"></i>
                            </button>--}}
                        </td>
                    </tr>
                @endforeach
              {{--  <div class="row form-group pull-right">
                    {{$list_price->links()}}
                </div>--}}
            </table>
        </section>
        <!-- /.content -->

        <div class="modal" ref="price" id="price">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" action="{{route('admin_create_price')}}">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <h4 class="modal-title">Thêm mới khoảng giá</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label>Tên khoảng giá điện</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="name"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label>Số điện min</label>
                                </div>
                                <div class="col-md-8">
                                    <input name="min" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label>Số điện min</label>
                                </div>
                                <div class="col-md-8">
                                    <input name="max" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label>Giá</label>
                                </div>
                                <div class="col-md-8">
                                    <input name="price" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary btn-add-price">Thêm</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <div class="modal" ref="price" id="update-price">
        </div>
    </div>
@endsection

@section('admin_script')
    <script>
        $(document).ready(function () {
            $('#add-price').on('click', function (e) {
                $('#price').modal();
            });

            $('.btn__delete').on("click", function (e) {
                let price_id = $(this).attr("data-price_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_delete_price')}}",
                        data: {
                            id: price_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            console.log(xhr);
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });

            $('.btn__edit').on("click", function (e) {
                let price_id = $(this).attr("data-price_id");
                $.ajax({
                    type: "GET",
                    url:  "update-price/" + price_id,
                    dataType: "json",
                    success: function (result) {
                        if (result.code === 1) {
                            $('#update-price').html(result.data);
                            $('#update-price').modal();
                        } else {
                            toastError(result.msg);
                        }
                    },
                    error: function (xhr) {
                        toastError(xhr.responseJSON.msg);
                    }
                });
            });
        });
    </script>
@endsection
