<?php

namespace App\Repositories;

use App\Models\Post;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class PostRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\Post';
    }

    public function getList($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;
        return Post::where(function ($query) use ($params) {
                if (!empty($params['keyword'])) {
                    $query->where('name','like','%' . $params['keyword'] . '%');
                }

                if (!empty($params['status']) || isset($params['status'])) {
                    $query->where('status', '=', intval($params['status']));
                }
            })->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'asc')->get();
    }

    public function getByID($device_id)
    {
        return Post::where('id', '=', $device_id)->first();
    }

    public function findByPost($device_name)
    {
        return Post::where('name', '=', $device_name)->first();
    }

    /**
     * @param $params
     * @return Post
     * @throws Exception
     */
    public function createPost($params)
    {
        try {
            DB::beginTransaction();
            $params['created_at'] = time();
            $params['updated_at'] = time();
            $device = $this->create($params);
            DB::commit();
            return $device;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB');
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return Post
     * @throws Exception
     */
    public function updatePost($id, $params = [])
    {
        try {
            $params['updated_at'] = time();
            $updated_device = $this->update($params, $id);
            return $updated_device;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deletePost($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
