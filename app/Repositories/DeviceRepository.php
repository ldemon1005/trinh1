<?php

namespace App\Repositories;

use App\Models\DeviceName;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class DeviceRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\DeviceName';
    }

    public function getList($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;
        return DeviceName::where(function ($query) use ($params) {
                if (!empty($params['keyword'])) {
                    $query->where('name','like','%' . $params['keyword'] . '%');
                }

                if (!empty($params['status']) || isset($params['status'])) {
                    $query->where('status', '=', intval($params['status']));
                }
            })->orderBy($params['order_by'] ?? 'name', $params['order_direction'] ?? 'desc')->paginate($limit);
    }

    public function getByID($device_id)
    {
        return DeviceName::where('id', '=', $device_id)->first();
    }

    public function findByDeviceName($device_name)
    {
        return DeviceName::where('name', '=', $device_name)->first();
    }

    /**
     * @param $params
     * @return DeviceName
     * @throws Exception
     */
    public function createDeviceName($params)
    {
        try {
            DB::beginTransaction();
            $params['created_at'] = time();
            $params['updated_at'] = time();
            $device = $this->create($params);
            DB::commit();
            return $device;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB');
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return DeviceName
     * @throws Exception
     */
    public function updateDeviceName($id, $params = [])
    {
        try {
            $params['updated_at'] = time();
            $updated_device = $this->update($params, $id);
            return $updated_device;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteDeviceName($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
