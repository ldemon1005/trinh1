@extends('client.master')

@section('content')
    <div id="content-main">
        <div class="container">
            <div class="navigator">
                <a href="/">Trang chủ</a>»<span>Tính toán điện năng tiêu thụ</span>
            </div>
            <div>
                <span class="create-invoice" style="line-height: 50px; color: #39a5d5; text-decoration: underline; cursor: pointer" onclick="createInvoice()">Tạo hóa đơn</span>
            </div>
            <div class="invoice-info hidden">
                <div class="printer">
                    <div class="row">
                        <div class="col-md-6">
                            <span class="show-template-invoice" style="line-height: 50px; color: #39a5d5; text-decoration: underline; cursor: pointer" onclick="showTemplateInvoice()">Xem mẫu hóa đơn</span>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="pointer" @click="printBill()">
                                <img src="{{asset('client/imgs/printer.png')}}" alt="printer">
                                <span>In hóa đơn</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="template-invoice hidden">
                    <img src="{{asset('client/imgs/example_invoice.png')}}" alt="example invoice">
                </div>
                <session class="box-content box-customer-info" style="margin-bottom: 50px; display: block">
                    <div class="title">
                        <h3 style="background: #fff;">Thông tin hóa đơn</h3>
                    </div>
                    <div class="content-page box-ttdn">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="ky">Kỳ</label>
                                    <input type="text" class="form-control" id="ky" v-model="info_bill.period" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="start_time">Từ ngày:</label>
                                    <input type="text" class="form-control" id="start_time" v-model="info_bill.from_date" aria-describedby="emailHelp" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="number_1">Đến ngày:</label>
                                    <input type="text" class="form-control" id="number_1" v-model="info_bill.to_date" aria-describedby="emailHelp" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="track">Mã HĐ:</label>
                                    <input type="text" class="form-control" id="track" v-model="info_bill.code_contract" aria-describedby="emailHelp" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                </session>
                <session class="box-content box-customer-info" style="margin-bottom: 50px; display: block">
                    <div class="title">
                        <h3 style="background: #fff;">Thông tin khách hàng</h3>
                    </div>
                    <div class="content-page box-ttdn">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="fullname">Tên khách hàng:</label>
                                    <input type="text" class="form-control" id="fullname" v-model="info_bill.customer_name" aria-describedby="emailHelp" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="code_customer">Mã khách hàng:</label>
                                    <input type="text" class="form-control" id="code_customer" v-model="info_bill.customer_code" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="code_area">Mã trạm:</label>
                                    <input type="text" class="form-control" id="code_area" v-model="info_bill.code_1" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="mst">MST:</label>
                                    <input type="text" class="form-control" id="mst" v-model="info_bill.tax_code" aria-describedby="emailHelp" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="level">Cấp ĐA:</label>
                                    <input type="text" class="form-control" id="level" v-model="info_bill.level" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="code_price">Mã giá:</label>
                                    <input type="text" class="form-control" id="code_price" v-model="info_bill.code_price" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="number_1">Số công tơ:</label>
                                    <input type="text" class="form-control" id="number_1" v-model="info_bill.number_meters" aria-describedby="emailHelp" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="code_nn">Mã NN:</label>
                                    <input type="text" class="form-control" id="code_nn" v-model="info_bill.code_nn" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="number_GCS">Số GCS:</label>
                                    <input type="text" class="form-control" id="number_GCS" v-model="info_bill.number_gcs" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="code_family">Số hộ:</label>
                                    <input type="text" class="form-control" id="code_family" v-model="info_bill.number_households" aria-describedby="emailHelp" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="code_group">Mã tổ:</label>
                                    <input type="text" class="form-control" id="code_group" v-model="info_bill.code_group" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="pgcs">PGCS:</label>
                                    <input type="text" class="form-control" id="pgcs" v-model="info_bill.pgcs" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                </session>
            </div>
            <session class="box-content ">
                <div class="title">
                    <h3>Tính toán điện năng trung bình</h3>
                </div>
                <div class="content-page box-ttdn">
                    <div class="table-responsive">
                        <div class="table" style="min-width: 1024px">
                            <div class="row form-group box-ttdn_header">
                                <div class="col-1">Địa điểm sử dụng</div>
                                <div class="col-1">Mục đích sử dụng</div>
                                <div class="col-1">Tên loại thiết bị</div>
                                <div class="col-1">Công suất <br/> (P/W)</div>
                                <div class="col-1">Số lượng <br/> (Cái)</div>
                                <div class="col-2">Thời gian sử dụng TB trong ngày <br> (Giờ)</div>
                                <div class="col-2">Điện năng tiêu thụ TB trong ngày</div>
                                <div class="col-2">Thời gian sử dụng TB trong tháng <br> (Ngày)</div>
                                <div class="col-1">Điện năng tiêu thụ trong tháng (w)</div>
                            </div>
                            <div class="calculator" v-for="(items, index1) in bill">
                                <div v-if="items.length == 0" class="row form-group">
                                    <div class="col-1 area">
                                        @{{ location[index1] }}
                                        <i @click="delete_location(index1)" title="Xóa địa điểm" class="fas fa-trash-alt fa-5x"></i>
                                    </div>
                                    <div class="col-3">
                                        <button @click="add_device_modal(index1)" class="btn btn-primary">Thêm mới thiết bị
                                        </button>
                                    </div>
                                </div>
                                <div v-else v-for="(item,index2) in items" class="row form-group">
                                    <div class="col-1 area">
                                        @{{ index2 == 0 ? location[item.location] : '' }}
                                        <i v-if="index2 == 0" @click="delete_location(index1)" title="Xóa địa điểm" class="fas fa-trash-alt fa-5x"></i>
                                    </div>
                                    <div class="col-1 name-device">
                                        @{{ item.target_use }}
                                        <i @click="delete_devicce(index1, index2)" title="Xóa thiết bị" class="fas fa-trash fa-5x"></i>
                                    </div>
                                    <div class="col-1">@{{ item.device_name }}</div>
                                    <div class="col-1">
                                        <input @change="changeTotal()" class="form-control" type="text" v-model="item.capacity">
                                    </div>
                                    <div class="col-1">
                                        <input @change="changeTotal()" class="form-control" type="text" v-model="item.quantity">
                                    </div>
                                    <div class="col-2">
                                        <input @change="changeTotal()" class="form-control" type="text" v-model="item.time_per_day">
                                    </div>
                                    <div class="col-2">@{{ item.capacity * item.quantity * item.time_per_day }}</div>
                                    <div class="col-2">
                                        <input @change="changeTotal()" class="form-control" type="text" v-model="item.day_per_month">
                                    </div>
                                    <div class="col-1">@{{formatPrice(item.capacity * item.quantity * item.time_per_day * item.day_per_month) }}
                                    </div>
                                </div>
                                <div v-if="items.length != 0" class="row form-group">
                                    <div class="col-1"></div>
                                    <div class="col-3">
                                        <button @click="add_device_modal(index1)" class="btn btn-primary">Thêm mới thiết bị
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-8"></div>
                                <div class="col-2">
                                    <button @click="save_temp()" style="float: right;" class="btn btn-default">Lưu tạm</button>
                                </div>
                                <div class="col-2">
                                    <button @click="add_location_modal()" style="float: right;" class="btn btn-primary">Thêm địa điểm sử dụng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group total-bill">
                    <div class="col-12 col-md-7"></div>
                    <div class="col-12 col-md-5">
                        <div class="row form-group">
                            <div class="col-6">
                                <strong>Tổng điện năng sử dụng TB trong ngày (w)</strong>
                            </div>
                            <div class="col-6">
                                @{{ formatPrice(total_day) }}
                            </div>
                        </div>
                        <div class="row form-group total-price">
                            <div class="col-6">
                                <strong>Tổng điện năng tiêu thụ trong tháng (w) / Tổng tiền chưa thuế</strong>
                            </div>
                            <div class="col-6">
                                @{{ formatPrice(total_month) + ' / ' + formatPrice(total_money)}} vnđ
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-1 area">Tổng</div>--}}
                    {{--<div class="col-1 name-device"></div>--}}
                    {{--<div class="col-1"></div><div class="col-1">--}}
                    {{--</div><div class="col-1"></div>--}}
                    {{--<div class="col-2"></div>--}}
                    {{--<div class="col-2">@{{ total_day }}</div>--}}
                    {{--<div class="col-2"></div>--}}
                    {{--<div class="col-1">@{{ total_month + ' / ' + total_money}} vnd</div>--}}
                </div>
            </session>
        </div>

        <div class="modal" ref="add_device" id="add_device">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Thêm thiết bị</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label>Địa điểm sử dụng</label>
                            </div>
                            <div class="col-md-8">
                                <select class="custom-select" v-model="device.location">
                                    <option v-for="(item, index) in location" :value="index" disabled>@{{ item }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label>Mục đích sử dụng</label>
                            </div>
                            <div class="col-md-8">
                                <select class="custom-select" v-model="device.target_use">
                                    @foreach($listTarget as $key => $target)
                                        <option {{$key == 0 ? 'selected' : ''}}> {{$target->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label>Tên, loại thiết bị</label>
                            </div>
                            <div class="col-md-8">
                                <select class="custom-select" v-model="device.device_name" @change="changeDevice()">
                                    @foreach($listDevice as $key => $device)
                                        <option {{$key == 0 ? 'selected' : ''}}> {{$device->name}} </option>
                                    @endforeach
                                </select>
                                <a href="javascript:void(0)" class="text-add-device" data-toggle="modal" @click="add_type_device()"
                                   data-target="#device">
                                    Thêm tên, loại thiết bị
                                </a>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label>Công suất</label>
                            </div>
                            <div class="col-md-8">
                                <input v-model="device.capacity" type="text" class="form-control"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label>Số lượng</label>
                            </div>
                            <div class="col-md-8">
                                <input v-model="device.quantity" type="text" class="form-control"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label>Số giờ sử dụng TB / 1 ngày</label>
                            </div>
                            <div class="col-md-8">
                                <input v-model="device.time_per_day" type="text" class="form-control"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label>Số ngày sử dụng TB / 1 tháng</label>
                            </div>
                            <div class="col-md-8">
                                <input v-model="device.day_per_month" type="text" class="form-control"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <button @click="add_device()" type="button" class="btn btn-primary btn-add-device">
                                    Thêm
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" ref="device" id="type_device">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" action="{{route('client_create_device')}}">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <h4 class="modal-title">Thêm loại thiết bị</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <input hidden value="client" name="create_from">
                        <input hidden value="0" name="status">
                        <div class="modal-body">
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label>Tên, loại thiết bị</label>
                                </div>
                                <div class="col-md-8">
                                    <input name="name" type="text" class="form-control"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label>Mức độ tiêu thụ</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="capacity" />
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-add-device">Thêm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal" ref="location" id="location">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Thêm khu vực</h4>
                        <button type="button" @click="closeModalLocation()" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label>Tên khu vực</label>
                            </div>
                            <div class="col-md-8">
                                <input v-model="location_new" type="text" class="form-control"/>
                            </div>
                        </div>
                        <div class="row" v-if="location_new_error != ''">
                            <small style="color: red;">@{{ location_new_error }}</small>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" @click="closeModalLocation()" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="button" @click="add_location()" class="btn btn-primary btn-add-device">Thêm</button>
                    </div>

                </div>
            </div>
        </div>


        <div hidden>
            <div id="content-bill">
                <div class="container" style="  max-width: 1024px; margin: auto; background: #F9FDFE;">
                <div class="invoice-content">
                    <div class="invoice-content_header" style="display:flex;">
                        <div class="logo" style="width: 30%;">
                            <div class="cover-img">
                                <img src="http://sangkienpchoabinh.com/client/imgs/logo_NPC.gif" alt="logo" style="width: 85%;">
                            </div>
                        </div>
                        <div class="invoice-name" style="width: 40%; text-align: center">
                            <h3 style="color: #3b81ec; text-transform: uppercase; font-size: 20px; margin-bottom: 7px; font-weight: normal; margin-top: 0; text-align: center">hóa đơn gtgt (tiền điện)</h3>
                            <strong style="color: #3b81ec">(Bản thể hiện của hóa đơn điện tử)</strong><br>
                            <div class="time">
                                <label style="color: #3b81ec; margin-right: 7px;" for="moth">Kỳ: <span style="color: #000" id="moth">@{{ info_bill.period }}</span> </label>
                                <label style="color: #3b81ec; margin-right: 7px;" for="from_date">Từ ngày: <span style="color: #000" id="from_date"> @{{ info_bill.from_date }}</span> </label>
                                <label style="color: #3b81ec; margin-right: 7px;  margin-right: 0;" for="to_date">Kỳ: <span style="color: #000" id="to_date">@{{ info_bill.to_date }}</span> </label>
                            </div>
                        </div>
                        <div class="info" style="width: 30%;">
                            <div style="padding-left: 100px;">
                                <label style="color: #3b81ec;" class="info">Mẫu số: 01GTKKT0/001</label><br>
                                <label style="color: #3b81ec;" class="info">Ký hiệu: <strong style="color: #000">NN/19E</strong></label><br>
                                <label style="color: #3b81ec;" class="info">Số: 0228804</label><br>
                                <label style="color: #3b81ec;" class="info">ID HĐ: @{{ info_bill.code_contract }}</label>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="invoice-content_company">
                         <h4>Công ty Điện lực Hòa Bình - Điện lực Thành phố Hòa Bình</h4>
                         <label>Địa chỉ: <span style="color: #000">Phường Đồng Tiến - TX Hòa Bình</span></label>
                         <label>Điện thoại: 19006769</label>
                         <div class="phones-company d-flex">
                             <label class="w-33">Điện thoại: 19006769</label>
                             <label class="w-33">MST: <span style="color: #000">0100100417-019</span></label>
                             <label class="w-33">ĐT sửa chữa: 19006769</label>
                         </div>
                     </div>-->
                    <div class="invoice-content_customer">
                        <h3 style="font-weight: bold; color: #1177e4; margin-top: 0; margin-bottom: 0">Tên khách hàng: <strong style="color: #000;">@{{ info_bill.customer_name }}</strong></h3>
                        <label>Địa chỉ: <span style="color: #000">Tổ 22 P.Tân Thịnh TP Hòa Bình</span></label>
                        <div class="box-info-customer" style="display: flex">
                            <div class="box-info-item" style="width: 25%">
                                <label style="color: #3b81ec">Điện thoại: <span style="color: #000">121321321321</span></label><br>
                                <label style="color: #3b81ec">Mã KH: <span style="color: #000">@{{ info_bill.customer_code }}</span></label><br>
                                <label style="color: #3b81ec">Mã trạm: <span style="color: #000">45454654</span></label><br>
                            </div>
                            <div class="box-info-item" style="width: 25%">
                                <label style="color: #3b81ec">MST: <span style="color: #000">@{{ info_bill.tax_code }}</span></label><br>
                                <label style="color: #3b81ec">Cấp ĐA: <span style="color: #000">@{{ info_bill.level }}</span></label><br>
                                <label style="color: #3b81ec">Mã giá: <span style="color: #000">KT: @{{ info_bill.code_price }}</span></label><br>
                            </div>
                            <div class="box-info-item" style="width: 25%">
                                <label style="color: #3b81ec">Số công tơ: <span style="color: #000">@{{ info_bill.number_meters }}</span></label><br>
                                <label style="color: #3b81ec">Mã NN: <span style="color: #000">@{{ info_bill.code_nn }}</span></label><br>
                                <label style="color: #3b81ec">Só GCS: <span style="color: #000">@{{ info_bill.number_gcs }}</span></label><br>
                            </div>
                            <div class="box-info-item" style="width: 25%">
                                <label style="color: #3b81ec">Số hộ: <span style="color: #000">@{{ info_bill.number_households }}</span></label><br>
                                <label style="color: #3b81ec">Mã tổ: <span style="color: #000">@{{ info_bill.code_group }}</span></label><br>
                                <label style="color: #3b81ec">P GCS: <span style="color: #000">@{{ info_bill.pgcs }}</span></label><br>
                            </div>
                        </div>
                    </div>
                    <div class="table-calculator" style="margin-top: 30px;">
                        <table style="border: 1px solid #cccccc;  width: 100%; border-collapse: collapse; border-spacing: 0;">
                            <tr>
                                <th style="border: 1px solid #cccccc;  padding: 7px;">Bộ CS</th>
                                <th style="border: 1px solid #cccccc;  padding: 7px;">Chỉ số mới</th>
                                <th style="border: 1px solid #cccccc;  padding: 7px;">Chỉ số cũ</th>
                                <th style="border: 1px solid #cccccc;  padding: 7px;">HS Nhân</th>
                                <th style="border: 1px solid #cccccc;  padding: 7px;">ĐN Tiêu thụ</th>
                                <th style="border: 1px solid #cccccc;  padding: 7px;">ĐN Trực tiếp</th>
                                <th style="border: 1px solid #cccccc;  padding: 7px;">ĐN trừ phụ</th>
                                <th style="border: 1px solid #cccccc;  padding: 7px;">ĐN thực tế</th>
                                <th style="border: 1px solid #cccccc;  padding: 7px;">Đơn giá</th>
                                <th style="border: 1px solid #cccccc;  padding: 7px;">Thành tiền</th>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #cccccc;  padding: 7px;">KT</td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;">6.791</td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;">6.304</td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;">1</td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;">487</td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;">0</td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;">0</td>
                                <td style="text-align: right">@{{ formatPrice(total_month / 1000) }}</td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;"></td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;"></td>
                            </tr>
                            <tr >
                                <td style="border: 1px solid #cccccc;  padding: 7px;"></td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;"></td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;"></td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;"></td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;"></td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;"></td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;"></td>
                                <td style="text-align: right; border: 1px solid #cccccc; padding: 7px;">
                                    <div v-for="(item,index) in listPrice"><span>@{{ formatPrice(item.number) }}</span></div>
                                </td>
                                <td style="text-align: right; border: 1px solid #cccccc; padding: 7px;">
                                    <div v-for="(item,index) in listPrice"><span>@{{ formatPrice(item.price) }}</span></div>
                                </td>
                                <td style="text-align: right; border: 1px solid #cccccc; padding: 7px;">
                                    <div v-for="(item,index) in listPrice"><span>@{{ formatPrice(item.totalPrice) }}</span></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #cccccc;  padding: 7px;"colspan="7">
                                    <label style="color: #3b81ec">Cộng</label>
                                </td>
                                <td style="text-align: right; border: 1px solid #cccccc; padding: 7px;"><b>@{{ formatPrice(total_month / 1000) }}</b></td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;"></td>
                                <td style="text-align: right; border: 1px solid #cccccc; padding: 7px;"><b>@{{ formatPrice(total_money) }}</b></td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #cccccc;  padding: 7px;" colspan="7">
                                    <label style="color: #3b81ec">Thuế suất GTGT: <span>10%</span></label>
                                </td>
                                <td style="border: 1px solid #cccccc;  padding: 7px;" colspan="2">
                                    <label style="color: #3b81ec">Thuế GTGT: </label>
                                </td>
                                <td style="text-align: right; border: 1px solid #cccccc; padding: 7px;">
                                    <strong>@{{ formatPrice(total_money * 0.1) }}</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #cccccc;  padding: 7px;" colspan="9">
                                    <label style="color: #3b81ec">Tổng tiền thanh toán:</label>
                                </td>
                                <td style="text-align: right; border: 1px solid #cccccc; padding: 7px;">
                                    <strong>@{{ formatPrice(total_money * 1.1) }}</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #cccccc;  padding: 7px;" colspan="10">
                                    <label style="color: #3b81ec">Số tiền bằng chữ: <i style="font-style: italic; color: #000;"></i></label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

    <script>

        function createInvoice() {
            if ($('.invoice-info').hasClass('hidden')) $('.invoice-info').removeClass('hidden');
            $('.create-invoice').addClass('hidden');
        }

        function showTemplateInvoice() {
            if ($('.template-invoice').hasClass('hidden')) {
                $('.template-invoice').removeClass('hidden');
                $('.show-template-invoice').text('Ẩn hóa đơn mẫu');
            } else {
                $('.template-invoice').addClass('hidden');
                $('.show-template-invoice').text('Xem hóa đơn mẫu');
            }
        }

        new Vue({
            el: "#content-main",
            data() {
                return {
                    location_new: '',
                    location_new_error: '',
                    location: [
                        'Phòng khách', 'Sân vườn, Hành lang', 'Công trình khác', 'Phòng ngủ', 'Nhà vệ sinh', 'Nhà bếp'
                    ],
                    device: {
                        location: 0,
                        target_use: 0,
                        device_name: 0,
                        capacity: '',
                        quantity: '',
                        time_per_day: '',
                        day_per_month: ''
                    },
                    listDevice: [],
                    price: [],
                    bill: [
                        [],
                        [],
                        [],
                        [],
                        [],
                        [],
                    ],
                    total_day: 0,
                    total_month: 0,
                    total_money: 0,
                    listPrice: '',
                    info_bill: {
                        period: '',
                        from_date: '',
                        to_date: '',
                        code_contract: '',
                        customer_name: '',
                        tax_code: '',
                        number_meters: '',
                        number_households: '',
                        customer_code: '',
                        code_1: '',
                        code_2: '',
                        code_nn: '',
                        level: '',
                        number_gcs: '',
                        pgcs: '',
                        code_price: '',
                        code_group: ''
                    }
                }
            },
            methods: {
                add_device_modal(index) {
                    this.device.location = index;
                    $("#add_device").modal();
                },
                add_device() {
                    this.bill[this.device.location].push(this.device);
                    this.device = {
                        location: 0,
                        target_use: 0,
                        device_name: 0,
                        capacity: '',
                        quantity: '',
                        time_per_day: '',
                        day_per_month: ''
                    };
                    $("#add_device").modal("hide");
                    toastSuccess('Thêm thiết bị thành công');
                },
                add_location_modal() {
                    $('#location').modal();
                },
                closeModalLocation() {
                    this.location_new = '';
                    this.location_new_error = '';
                    $('#location').modal('hide');
                },
                add_location() {
                    var index = this.location.indexOf(this.location_new);
                    if (index < 0) {
                        this.location.push(this.location_new);
                        this.location_new = '';
                        this.location_new_error = '';
                        this.bill.push([]);
                        $('#location').modal('hide');
                        toastSuccess('Thêm khu vực thành công');
                    } else {
                        toastSuccess('Khu vực đã được thêm!');
                        this.location_new_error = 'Khu vực đã được thêm!';
                    }
                },
                delete_location(index) {
                    this.location.splice(index, 1);
                    this.bill.splice(index, 1);
                    toastSuccess('Xóa khu vực thành công');
                },
                delete_devicce(index1, index2) {
                    this.bill[index1].splice(index2, 1);
                    toastSuccess('Xóa thiết bị thành công');
                },
                save_temp() {
                    localStorage.setItem('location', JSON.stringify(this.location));
                    localStorage.setItem('bill', JSON.stringify(this.bill));
                    localStorage.setItem('total_day', this.total_day);
                    localStorage.setItem('total_month', this.total_month);
                    toastSuccess('Lưu thành công');
                },
                changeTotal() {
                    let day = 0;
                    let month = 0;
                    let money = 0;
                    if (this.bill.length) {
                        $.each(this.bill, function (key, items) {
                            if (items.length) {
                                $.each(items, function (key, value) {
                                    day += value.capacity * value.quantity * value.time_per_day;
                                    month += value.capacity * value.quantity * value.time_per_day * value.day_per_month;
                                });
                            }
                        });
                    }
                    console.log('asdasdasdasd', month/1000);
                    let totalNumber = parseInt(month/1000);

                    let listPrice = [];

                    this.price.every((item, index) => {

                        /*if (index == this.price.length - 1) {
                            money += item.price * (totalNumber - item.min + 1);
                            return false;
                        }
                        if (totalNumber < item.max) {
                            money += item.price * (totalNumber - item.min + 1);
                            return false;
                        } else {
                            money += item.price * (item.max - item.min + 1);
                        }*/

                        if (totalNumber <= 0) return;

                        let obj = {};1
                        obj.price = item.price;
                        if (totalNumber >= item.max) {
                            money += item.max * item.price;
                            obj.number = item.max;
                        } else {
                            money += totalNumber * item.price;
                            obj.number = totalNumber;
                        }
                        obj.totalPrice = parseInt(obj.price) * parseInt(obj.number);
                        listPrice.push(obj);
                        totalNumber -= item.max;
                        return true;
                    });

                    this.total_day = day;
                    this.total_month = month;
                    this.total_money = money;
                    this.listPrice = listPrice;
                },
                changeDevice(){
                    this.listDevice.forEach(x=>{
                        if(x.name == this.device.device_name){
                            this.device.capacity = x.capacity;
                        }
                    })
                },
                printBill() {
                    var mywindow = window.open('', 'PRINT', 'height=1000,width=1200');

                    // mywindow.document.write('<html><head><title>' + 'Hóa đơn điện' + '</title>');
                    mywindow.document.write('<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">');
                    mywindow.document.write('<link href="{{asset('client/css/custom.css')}}" rel="stylesheet">');
                    mywindow.document.write('</head><body >');
                    // mywindow.document.write('<h1>' + 'Hóa đơn điện' + '</h1>');
                    mywindow.document.write(document.getElementById('content-bill').innerHTML);
                    mywindow.document.write('</body></html>');

                    mywindow.document.close(); // necessary for IE >= 10
                    mywindow.focus(); // necessary for IE >= 10*/

                    mywindow.print();
                    mywindow.close();

                    return true;
                },
                add_type_device() {
                    $('#type_device').modal();
                },
                formatPrice(value) {
                    let val = (value / 1).toFixed(0).replace('.', ',')
                    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                },
            },
            mounted() {
                if (localStorage.getItem('location')) {
                    this.location = JSON.parse(localStorage.getItem('location'));
                }

                if (localStorage.getItem('bill')) {
                    this.bill = JSON.parse(localStorage.getItem('bill'));
                }

                this.price = {!! $listPrice !!};
                this.listDevice = {!! $listDevice1 !!}.data;
            },
            watch: {
                bill: function (val) {
                    let day = 0;
                    let month = 0;
                    let money = 0;
                    if (val.length) {
                        $.each(val, function (key, items) {
                            if (items.length) {
                                $.each(items, function (key, value) {
                                    day += value.capacity * value.quantity * value.time_per_day;
                                    month += value.capacity * value.quantity * value.time_per_day * value.day_per_month;
                                });
                            }
                        });
                    }

                    let totalNumber = parseInt(month/1000);

                    let listPrice = [];

                    this.price.every((item, index) => {
                       /* if (index == this.price.length - 1) {
                            money += item.price * (month - item.min + 1);
                            return false;
                        }
                        console.log(month < item.max, month, item.max)
                        if (month < item.max) {
                            money += item.price * (month - item.min + 1);
                            return false;
                        } else {
                            money += item.price * (item.max - item.min + 1);
                        }*/
                        let distance = item.max - item.min + 1;
                        if (totalNumber <= 0) return;

                        let obj = {};
                        obj.price = item.price;
                        if (totalNumber >= distance) {
                            money += distance * item.price;
                            obj.number = distance;
                        } else {
                            money += totalNumber * item.price;
                            obj.number = totalNumber;
                        }
                        obj.totalPrice = parseInt(obj.price) * parseInt(obj.number);
                        listPrice.push(obj);
                        totalNumber = totalNumber - distance;
                        return true;
                    });

                    this.total_day = day;
                    this.total_month = month;
                    this.total_money = money;
                    this.listPrice = listPrice;
                }
            }
        })
    </script>
@endsection

