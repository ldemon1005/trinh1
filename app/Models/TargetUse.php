<?php

namespace App\Models;

class TargetUse extends BaseModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'target_use';
}
