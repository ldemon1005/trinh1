@extends('client.master')

@section('content')
    <div id="content-main">
        <div class="container">
            <div class="navigator">
                <a href="/" class="navigator-home" style="margin-left: 0">Trang chủ</a>
            </div>
            <session class="box-content">
                <div class="content-page">
                    <div class="row main-content" style="margin-left: 0; border: 1px solid #ccc; margin-right: 0">
                        <div class="banner" style="padding-right: 0; width: 100%">
                            <img src="{{asset('client/imgs/2_1.jpg')}}" alt="right banner">
                        </div>
                    </div>
                    <div class="row">
                        <div class="cskh">
                            <img src="{{asset('client/imgs/CSKH.jpg')}}" alt="cskh">
                        </div>
                    </div>
                </div>
            </session>
        </div>
    </div>
@endsection

