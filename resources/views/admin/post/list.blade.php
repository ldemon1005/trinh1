@extends('admin.master')

@section('admin_main')
    @include('admin.layout.breadcrumb')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-info" onclick="location.href = '{{ route("admin_post_new_view") }}'">Thêm mới</button>
                        <div class="pull-right">
                            <form action="{{route('admin_post_list_view')}}" method="GET">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Nhập từ khoá tìm kiếm..." value="{{isset($params['keyword']) ? $params['keyword'] : ''}}">
                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <div id="postTable_wrapper" class="dataTables_wrapper no-footer">
                                <!-- Table -->
                                <table id="postTable" class="table table-bordered" style="min-width: 1200px;">
                                    <thead>
                                    <tr>
                                        <th>Ảnh</th>
                                        <th>Tiêu đề</th>
                                        <th>Chuyên mục</th>
                                        <th>Tác giả</th>
                                        <th width="100px">Trạng thái</th>
                                        <th width="180px">Ngày tạo</th>
                                        <th width="120px"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($list_post))
                                        @foreach($list_post as $post_item)
                                            <tr>
                                                <td><img height="30" class="image_preview" src="{{$post_item['image'] ? $post_item['image'] : asset('img/placeholder.png')}}" alt="{{$post_item['title']}}" title="{{$post_item['title']}}"></td>
                                                <td><a href="{{route('admin_post_update_view', ['post_id' => $post_item['id']])}}" class="text-dark">{{$post_item['title']}}</a></td>
                                                <td width="300px">{{$post_item['list_cat']}}</td>
                                                <td>{{$post_item->author->first_name}}</td>
                                                <td align="center">
                                                    <button type="button" class="btn btn-{{$post_item['status'] ? 'success' : 'danger'}} btn-rounded btn-sm" data-toggle="tooltip" title="{{$post_item['status'] ? 'Hiện' : 'Ẩn'}}">
                                                        <i class="fa fa-{{$post_item['status'] ? 'eye' : 'eye-slash'}}"></i>
                                                    </button>
                                                </td>
                                                <td>{{$post_item->getFormattedDate('created_at')}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-success btn-rounded btn-sm btn__edit" data-toggle="tooltip" title="Chỉnh sửa"
                                                            onclick="window.location.href = '{{route('admin_post_update_view', ['post_id' => $post_item['id']])}}'">
                                                        <i class="fa fa-pencil"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-rounded btn-sm btn__delete" data-post_id="{{$post_item['id']}}" data-toggle="tooltip" title="Xoá">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    <a href="{{route('client_post_detail_view', ['slug' => $post_item['slug']])}}" class="btn btn-info btn-rounded btn-sm" data-toggle="tooltip" title="Xem" target="_blank">
                                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="7"><strong>KHÔNG CÓ DỮ LIỆU!</strong></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!-- End Table -->
                            </div>
                        </div>
                        @if(count($list_post))
                            {{ $list_post->appends($params)->links('admin.layout.pagination', ['list_object' => $list_post]) }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn__delete').on("click", function (e) {
                let post_id = $(this).attr("data-post_id");
                alertWarning(function () {
                    $.ajax({
                        type: "POST",
                        url: "{{route('admin_post_delete')}}",
                        data: {
                            id: post_id,
                            _token: "{{csrf_token()}}"
                        },
                        dataType: "json",
                        success: function (result) {
                            if (result.code === 1) {
                                toastSuccess(result.msg);
                                window.location.reload();
                            } else {
                                toastError(result.msg);
                            }
                        },
                        error: function (xhr) {
                            toastError(xhr.responseJSON.msg);
                        }
                    });
                }, "Bạn có thực sự muốn xoá?");
            });
        });
    </script>
@endsection

