<?php

namespace App\Http\Controllers\Admin;

use App\Models\BaseModel;
use App\Repositories\TargetRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TargetController extends Controller
{
    protected $targetRepository;

    public function __construct(TargetRepository $targetRepository)
    {
        $this->targetRepository = $targetRepository;
    }

    public function listView(Request $request){
        $params = [
            'keyword' => '',
        ];
        $query = $request->only('keyword', 'status');
        $params = array_merge($params, $query);
        if(isset($params['status']) && $params['status'] == 9){
            unset($params['status']);
        }
        $list_target = $this->targetRepository->getList($params);
        return view('admin.target.list_view', compact('list_target','params'));
    }

    public function updateTargetView($id){
        $target = $this->targetRepository->getByID($id);

        if(!$target){
            $this->resFail(null, 'Không tìm thấy thiết bị');
        }

        $content = view('admin.target.form', compact('target'))->render();

        return $this->resSuccess($content, '');
    }

    public function createTarget(Request $request){
        $validator = Validator::make($request->input(), [
            'name' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }

        $target_data = $request->only('name','status');

        if(isset($target_data['status']) && $target_data['status'] == 'on'){
            $target_data['status'] = 1;
        }else{
            $target_data['status'] = 0;
        }

        try{
            $target = $this->targetRepository->createTarget($target_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Tạo mới không thành công!');
        }

        if(!$target){
            return redirect()->back()->withErrors('Tạo mới không thành công!');
        }

        return redirect()->route('admin_list_target')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công!');
    }

    public function updateTarget(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required',
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            redirect()->back()->withErrors($validator->errors());
        }

        $target_data = $request->only('id','name','status');

        $target_id = $request->get('id');

        unset($target_data['id']);

        if(isset($target_data['status']) && $target_data['status'] == 'on'){
            $target_data['status'] = 1;
        }else{
            $target_data['status'] = 0;
        }

        try{
            $target = $this->targetRepository->updateTarget($target_id, $target_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        if(!$target){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        return redirect()->route('admin_list_target')->with(BaseModel::ALERT_SUCCESS, 'Update thành công!');
    }

    public function deleteTarget(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }
        $target_data = $request->only('id');

        try {
            $target_deleted = $this->targetRepository->deleteTarget($target_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$target_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($target_deleted, 'Xoá thành công!');

    }
}
