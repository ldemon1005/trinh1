@extends('admin.master')

@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form  action="{{route('update-lap-phuong-an-tiet-kiem-dien')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input hidden name="id" value="{{$post->id}}">
            <div class="row">
                <div id="Post_Content" class="col-12" style="margin: auto; background: #fff; padding-right: 20px; padding-left: 20px">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN BÀI LẬP PHƯƠNG ÁN TIẾT KIỆM ĐIỆN</h4>
                            <div class="form-group">
                                <label for="post_title">Tiêu đề <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="title" id="post_title" aria-describedby="helpPostTitle" placeholder="Nhập tiêu đề bài viết"
                                       autocomplete="off" value="{{$post->title}}" required>
                                <small id="helpPostTitle" class="form-text text-muted">Tiêu đề bài viết là bắt buộc</small>

                            </div>

                            <div class="form-group">
                                <label for="post_content">Nội dung</label>
                                <textarea class="form-control mce_editor" name="content" id="post_content" rows="3">{{$post->content}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="post_description">Mô tả ngắn</label>
                                <textarea class="form-control" name="description" id="post_description" rows="3">{{$post->description}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">CẤU HÌNH SEO</h4>
                            <div class="row form-group">
                                <label class="col-md-2 text-left" for="post_seo_title">SEO Tiêu đề</label>
                                <div class="col-md-10">
                                    <input type="text" name="seo_title" id="post_seo_title" class="form-control" maxlength="70"
                                           placeholder="" value="{{$post->seo_title}}" autocomplete="off">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2 text-left" for="post_seo_title">SEO Mô tả</label>
                                <div class="col-md-10">
                                   <textarea name="seo_description" id="post_seo_description" class="form-control" rows="3"
                                             maxlength="170" placeholder="" autocomplete="off">{{$post->seo_title}}</textarea>
                                </div>
                            </div><div class="row form-group">
                                <label class="col-md-2 text-left" for="post_seo_title">SEO Từ khóa</label>
                                <div class="col-md-10">
                                    <input type="text" name="seo_keyword" id="post_seo_keyword" class="form-control" data-role="tagsinput"
                                           placeholder="" value="{{$post->seo_title}}" autocomplete="off">
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body text-right" style="margin-top: 50px">
{{--                            <h4 class="card-title font-bold">THAO TÁC</h4>--}}

                            <button type="submit" class="btn btn-info"> Đăng</button>
                        </div>
                    </div>
                </div>
               {{-- <div id="Post_Sidebar" class="col-12 col-md-3">
                    ẢNH ĐẠI DIỆN
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ẢNH ĐẠI DIỆN</h4>
                            @include('admin.layouts.image_preview', ['input_name' => 'image', 'input_id' => 'post_image', 'input_image' => $post->seo_title])
                        </div>
                    </div>
                    ĐĂNG BÀI
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THAO TÁC</h4>

                            <button type="submit" class="btn btn-info"> Đăng</button>
                        </div>
                    </div>
                </div>--}}
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>
@endsection
