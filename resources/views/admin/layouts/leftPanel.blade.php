<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">THANH ĐIỀU KHIỂN</li>
            <li><a href="{{route('dashboard-v1')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li><a href="{{route('admin_list_device')}}"><i class="fa fa-desktop text-primary"></i> <span>Thiết bị</span></a></li>
            <li><a href="{{route('admin_list_target')}}"><i class="fa fa-bullseye text-red"></i> <span>Mục đích sử dụng</span></a></li>
            <li><a href="{{route('admin_list_price')}}"><i class="fa fa-usd text-primary"></i> <span>Giá điện</span></a></li>
            <li><a href="{{route('huong_dan')}}"><i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Bài viết hướng dẫn</span></a></li>
            <li><a href="{{route('tb_tiet_kiem_dien')}}"><i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Tư vấn tiết kiệm điện</span></a></li>
            <li><a href="{{route('lap-phuong-an-tiet-kiem-dien')}}"><i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Lập phương án tiết kiệm điện</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
