<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/** @var \Illuminate\Support\Facades\Route $router */
$router->group(['namespace' => 'Admin', 'prefix' => 'admin'], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */


    //<editor-fold desc="auth view">
    $router->get('/login', 'AuthController@loginView')->name('admin_auth_login_view');
    $router->get('/register', 'AuthController@registerView')->name('admin_auth_register_view');
    $router->get('/reset-password', 'AuthController@resetPassView')->name('admin_auth_reset_password_view');
    //</editor-fold>

    //<editor-fold desc="auth action">
    $router->post('/login-action', 'AuthController@loginAction')->name('admin_auth_login_action');
    $router->post('/register-action', 'AuthController@registerAction')->name('admin_auth_register_action');
    $router->post('/reset-password-action', 'AuthController@resetPasswordAction')->name('admin_auth_reset_password_action');
    $router->post('/logout', 'AuthController@logout')->name('admin_auth_logout');
    //</editor-fold>

    $router->group(['middleware' => ['auth.admin']], function ($router) {
        /** @var \Illuminate\Support\Facades\Route $router */
        //<editor-fold desc="device view">
        $router->get('/list-device', 'DeviceController@listView')->name('admin_list_device');
        $router->get('/update-device/{id}', 'DeviceController@updateDeviceView')->name('admin_update_device_view');
        //</editor-fold>

        //<editor-fold desc="device action">
        $router->post('/create-device', 'DeviceController@createDevice')->name('admin_create_device');
        $router->post('/update-device', 'DeviceController@updateDevice')->name('admin_update_device_action');
        $router->post('/delete-device', 'DeviceController@deleteDevice')->name('admin_delete_device');
        //</editor-fold>

        //<editor-fold desc="target use view">
        $router->get('/list-target', 'TargetController@listView')->name('admin_list_target');
        $router->get('/update-target/{id}', 'TargetController@updateTargetView')->name('admin_update_target_view');
        //</editor-fold>

        //<editor-fold desc="target action">
        $router->post('/create-target', 'TargetController@createTarget')->name('admin_create_target');
        $router->post('/update-target', 'TargetController@updateTarget')->name('admin_update_target_action');
        $router->post('/delete-target', 'TargetController@deleteTarget')->name('admin_delete_target');
        //</editor-fold>

        //<editor-fold desc="price use view">
        $router->get('/list-price', 'PriceController@listView')->name('admin_list_price');
        $router->get('/update-price/{id}', 'PriceController@updatePriceView')->name('admin_update_price_view');
        //</editor-fold>

        //<editor-fold desc="price action">
        $router->post('/create-price', 'PriceController@createPrice')->name('admin_create_price');
        $router->post('/update-price', 'PriceController@updatePrice')->name('admin_update_price_action');
        $router->post('/delete-price', 'PriceController@deletePrice')->name('admin_delete_price');
        //</editor-fold>

        //<editor-fold desc="support use view">
        $router->get('/huong-dan', 'PostController@postView')->name('huong_dan');
        $router->post('/update-huong-dan', 'PostController@postUpdate')->name('update_huong_dan');
        //loại thiết bị tiết kiệm điện
        $router->get('/tb-tiet-kiem-dien', 'PostController@postView1')->name('tb_tiet_kiem_dien');
        $router->post('/update-tb-tiet-kiem-dien', 'PostController@postUpdate1')->name('update_tb_tiet_kiem_dien');

        //loại thiết bị tiết kiệm điện
        $router->get('/lap-phuong-an-tiet-kiem-dien', 'PostController@postView2')->name('lap-phuong-an-tiet-kiem-dien');
        $router->post('/update-lap-phuong-an-tiet-kiem-dien', 'PostController@postUpdate2')->name('update-lap-phuong-an-tiet-kiem-dien');

        //</editor-fold>
    });
});

$router->group(['namespace' => 'Admin', 'prefix' => 'api', 'middleware' => ['auth.admin']], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */
    /** ADMIN API */
    $router->post('/uploadImage', 'ApiController@uploadImage')->name('admin_api_image_upload');
});

$router->group(['namespace' => 'Client'], function ($router) {
    /** @var \Illuminate\Support\Facades\Route $router */
    //<editor-fold desc="auth view">
    $router->get('/', 'IndexController@index')->name('index_view');
    $router->get('/tinh-toan-dien-nang', 'IndexController@device')->name('view_calculator');
    $router->get('/huong-dan-su-dung', 'IndexController@userManual')->name('view_user_manual');
    $router->get('/thiet-bi-tiet-kiem-dien', 'IndexController@userManual1')->name('view_tb');
    $router->get('/lap-phuong-an-tiet-kiem-dien', 'IndexController@userManual2')->name('view_plan_save_electric');
    //</editor-fold>
});

$router->post('/create-device-client', 'Admin\DeviceController@createDevice')->name('client_create_device');

$router::get('/dashboard-v1', 'HomeController@dashboardV1')->name('dashboard-v1');
