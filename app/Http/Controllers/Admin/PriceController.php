<?php

namespace App\Http\Controllers\Admin;

use App\Models\BaseModel;
use App\Models\Price;
use App\Repositories\PriceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PriceController extends Controller
{
    protected $priceRepository;

    public function __construct(PriceRepository $priceRepository)
    {
        $this->priceRepository = $priceRepository;
    }

    public function listView(Request $request){
        $params = [
            'keyword' => '',
        ];
        $query = $request->only('keyword', 'status');
        $params = array_merge($params, $query);
        if(isset($params['status']) && $params['status'] == 9){
            unset($params['status']);
        }
        $list_price = $this->priceRepository->getList($params);
        return view('admin.price.list_view', compact('list_price','params'));
    }

    public function updatePriceView($id){
        $price = $this->priceRepository->getByID($id);

        if(!$price){
            $this->resFail(null, 'Không tìm thấy thiết bị');
        }

        $content = view('admin.price.form', compact('price'))->render();

        return $this->resSuccess($content, '');
    }

    public function createPrice(Request $request){
        $validator = Validator::make($request->input(), [
            'name' => 'required',
            'min' => 'required | numeric',
            'max' => 'required | numeric',
            'price' => 'required | numeric',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $price_data = $request->only('name','min','max','price');

        if(!$this->validatePrice($price_data['min'], $price_data['max'])){
            return redirect()->back()->withErrors('Khoảng giá không hợp lệ( giá min không được lớn hơn mã và không được bé hơn giá lớn nhất của các giá điện đã tạo)!');
        }

        try{
            $price = $this->priceRepository->createPrice($price_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Tạo mới không thành công!');
        }

        if(!$price){
            return redirect()->back()->withErrors('Tạo mới không thành công!');
        }

        return redirect()->route('admin_list_price')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công!');
    }

    public function updatePrice(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required',
            'name' => 'required',
            'min' => 'required | numeric',
            'max' => 'required | numeric',
            'price' => 'required | numeric',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $price_data = $request->only('name','min','max','price');

        $price_id = $request->get('id');

        unset($price_data['id']);

        try{
            $price = $this->priceRepository->updatePrice($price_id, $price_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        if(!$price){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        return redirect()->route('admin_list_price')->with(BaseModel::ALERT_SUCCESS, 'Update thành công!');
    }

    function validatePrice($min,$max){
        if($max <= $min) {
            return false;
        }
        $price = DB::table('price')->orderByDesc('max')->first();
        if($min < $price->max){
            return false;
        }
        return true;
    }

    public function deletePrice(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }
        $price_data = $request->only('id');

        try {
            $price_deleted = $this->priceRepository->deletePrice($price_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$price_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($price_deleted, 'Xoá thành công!');

    }
}
